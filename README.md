# fortune-communism



## Bringing the struggle to your Terminal

Per request, an upload of the custom .dat files I made for fortune.

Quick & dirty upload for public consumption, so if you're reading this you likely came from the Twitter post.

Given this, you're gonna have to look up where the fortune directory is on your system - on my Arch systems it's in /usr/share/fortune; per the man page the default is /usr/share/games/fortune - and copy both *communism* & *communism.dat* to said folder. Then invoke with 'fortune communism'. Voila! Enjoy!

